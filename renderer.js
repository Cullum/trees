class Renderer {
  constructor(el) { this.el = el; }

  render(tree) { this.el.appendChild(this.renderNode(tree)); }

  renderNode(node) {
    const el = document.createElement("li");

    el.innerHTML = `<span>${node.char || "ROOT"}</span>`;

    const children = document.createElement("ul");
    children.className = "collapsed";

    el.onclick = (event) => {
      event.stopPropagation();
      children.classList.toggle("collapsed");
    }

    node.children.forEach((child) => {
      const rendered_node = this.renderNode(child);

      if (rendered_node) children.appendChild(rendered_node);
    });

    el.appendChild(children);

    return el;
  }
}
