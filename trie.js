class Trie {
  constructor() {
    this.children = [];
  }

  addWord(word) {
    const chars = word.split("");
    let cursor = this;

    chars.forEach((char) => {
      let node = cursor.children.find((child) => child.char === char);

      if (!node) {
        node = new Node(char);

        cursor.children.push(node)
      }

      cursor = node;
    });
  }
}

class Node {
  constructor(char) {
    this.char = char;
    this.children = [];
  }
}
